import routes from './app/routes/routes';
import { SERVER_LINK } from './app/init/constants';

routes().listen(SERVER_LINK.PORT, (error) => {
	if (error) return console.log(`Error: ${error}`);
	console.log(`Server listening on port ${SERVER_LINK.PORT}`);
});
