const PORT = 8081;

export const SERVER_LINK = {
	MAIN_URL: `http://localhost:${PORT}`,
	PORT,
};

export const ROUTES = {
	SIGN_IN: '/authorization',
	SIGN_UP: '/registration',
	SIGN_OUT: '/logout',
	CHECK_TOKEN: '/check_token',
	GET_SOFTWARE_LIST: '/software',
	CHANGE_BASKET_CONTENT: '/change_basket_content',
	CLEAR_BASKET_CONTENT: '/clear_basket_content',
	EMAIL_VERIFICATION: '/email_verify',
	SEND_ORDER_DATA: '/send_order_data',
};

export const REQUEST_METHODS = {
	POST: 'post',
	GET: 'get',
};

export const ERROR_TYPES = {
	WARNING: 'warn',
	FATAL: 'fatal',
};

export const RESPONSES = {
	SIGN_UP_ERROR: { status: 400, error: 'User with this login or email already exists' },
	SIGN_IN_ERROR: { status: 400, error: 'Not valid login or password' },
	TOKEN_ERROR: { status: 400, error: 'Access token expired' },
	DB_QUERY_ERROR: { status: 400, error: 'Db query error' },
	METHOD_NOT_FOUND: { status: 400, error: 'Method not found' },
	GET_SUCCESS_RESPONSE: (data = {}) => ({ status: 200, data }),
	GET_ERROR_RESPONSE: (error, status = 400) => ({ status, error }),
};

export const GET_CONTROLLER_ERROR = (error, type = ERROR_TYPES.WARNING, config = {}) => ({
	type, error, config,
});

export default function () { }
