import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

const expressServer = () => {
	const app = express();

	app.use(cors());
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use('/public/', express.static('public'));

	return app;
};

export default expressServer;
