import mysql from 'mysql2';
import { GET_CONTROLLER_ERROR, ERROR_TYPES } from './constants';

const config = {
	host: 'localhost',
	port: 3306,
	user: 'root',
	password: 'weakpasswordNO6669',
	database: 'online_software_store',
};

const connection = mysql.createPool(config).promise();

export const mysqlExtended = {
	...connection,
	query: (...args) => connection.query(...args).then(res => res).catch(e => {
		throw GET_CONTROLLER_ERROR(e, ERROR_TYPES.FATAL);
	}),
	execute: (...args) => connection.execute(...args).catch(e => {
		throw GET_CONTROLLER_ERROR(e, ERROR_TYPES.FATAL);
	}),
};

export default connection;
