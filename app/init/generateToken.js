import crypto from 'crypto';

export default async function generateToken() {
	const generate = () => new Promise(resolve => {
		crypto.randomBytes(48, (err, buffer) => {
			const token = buffer.toString('hex');
			resolve(token);
		});
	});

	const accessToken = await generate();

	return accessToken;
}
