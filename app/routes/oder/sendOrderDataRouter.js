import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';
import clearBasketContentRoute from '../basket/clearBasketContent';
import insertOrderData from './insertOrderData';
import getMessage from './getMessage';
import mailer from './mailer';

const sendOrderDataRouter = ({
	body: { body: { content, cost, orderNumber, customerInfo, paymentData } },
}) => {
	mailer(customerInfo.email, `Подтверждение вашего заказа №#${orderNumber}`,
		getMessage(content, cost, customerInfo, paymentData));

	return mysqlExtended
		.execute(`SELECT offers.id FROM offers WHERE code IN ('${content
			.map(item => item.code).join('\', \'')}')`)
		.then(([offers]) =>
			clearBasketContentRoute({ body: { body: { userName: customerInfo.userName } } })
				.then(() => {
					const products = offers
						.map((item, index) => `${item.id}-${content[index].count}`);
					insertOrderData(cost, products, orderNumber, customerInfo);
				}))
		.catch(e => { throw GET_CONTROLLER_ERROR(e); });
};

export default sendOrderDataRouter;
