import nodemailer from 'nodemailer';
import { CONFIG } from '../../init/config';

const mailer = (email, subject, message) => {
	const transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: { user: CONFIG.E_MAIL, pass: CONFIG.PASSWORD },
	});

	const options = {
		from: CONFIG.FROM,
		to: email,
		subject,
		html: message,
	};

	transporter.sendMail(options);
};

export default mailer;
