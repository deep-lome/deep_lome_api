const getMessage = (content, cost, customerInfo, paymentData) => {
	const orderContent = content.map(item => `
	<div><font size='2'><strong>Наименование товара:</strong> ${item.name}</font></div>
	<div><font size='2'><strong>Кол-во копий:</strong> ${item.count}</font></div>
	<div><font size='2'><strong>Артикул:</strong> ${item.code}</font></div>
	<div><font size='2'><strong>Сумма для всех копий:</strong> 
	${(item.price * item.count).toLocaleString()} руб</font></div>
	`);

	return `
	<font face="Helvetica">
	<div>Благодарим вас за ваш заказ, ${customerInfo.firstName}. 
	Для подтверждения вашего заказа мы в скором времени свяжемся с вами с вами по телефону, указанному вами при оформлении: 
	<u>${customerInfo.phone}</u>.<div>
	<div>Ниже вы можете просмотреть детали вашего заказа. При возникновении вопросов позвоните нам по телефону: 
	<u>+7(800) 555-35-35</u>.<div>
	<p/>
	<div><h3><b>Детали платежа</b></h3><div>
	<p/>
	<div><strong>Тип карты:</strong> ${paymentData.cardType}<div>
	<div><strong>Имя держателя карты:</strong> ${paymentData.cardName}<div>
	<div><strong>Номер карты:</strong> xxxx-xxxx-xxxx-${paymentData.cardNumber.slice(12, 16)}<div>
	<div><strong>Дата истечения срока действия:</strong> 
	${paymentData.expDate.slice(0, 2)}/${paymentData.expDate.slice(3, 7)}<div>
	<p/>
	<div><p><h3><b>Сводка заказа</b></h3></p><div>
	<p/>
	${orderContent.join('<p/>')}
	<p/>
	<div><h3><b>Итого: ${cost.toLocaleString()} руб</b></h3><div>
	</font>
	`;
};

export default getMessage;
