import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const insertOrderData = (cost, products, orderNumber, customerInfo) => {
	const query = `SELECT id FROM users WHERE login = '${customerInfo.userName}'`;

	return mysqlExtended
		.execute(query).then(([usersIdList]) => mysqlExtended
			.execute(`INSERT INTO orders (user_id, order_number, products, cost, first_name, last_name, phone, e_mail) 
			VALUES (${usersIdList[0].id}, ${orderNumber}, '${products.join(',')}', 
			${cost}, '${customerInfo.firstName}', '${customerInfo.lastName}', 
			'${customerInfo.phone}', '${customerInfo.email}')`)
			.catch(e => { throw GET_CONTROLLER_ERROR(e); }));
};

export default insertOrderData;
