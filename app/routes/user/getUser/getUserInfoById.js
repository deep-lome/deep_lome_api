import { mysqlExtended } from '../../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../../init/constants';

const getUserInfoById = (userId) => mysqlExtended
	.execute(`SELECT * FROM users WHERE id = ${userId}`)
	.then(([userInfoResults]) => ({
		userName: userInfoResults[0].login,
		firstName: userInfoResults[0].first_name,
		lastName: userInfoResults[0].last_name,
		email: userInfoResults[0].e_mail,
		phone: userInfoResults[0].phone,
		type: userInfoResults[0].type,
	}))
	.catch(e => { throw GET_CONTROLLER_ERROR(e); });

export default getUserInfoById;
