import { mysqlExtended } from '../../../init/connection';
import { RESPONSES, GET_CONTROLLER_ERROR } from '../../../init/constants';
import tokenVerification from '../token/tokenVerification';

const getUserInfoByToken = (token) => (
	tokenVerification(token).then((userId) => {
		if (!userId) {
			throw GET_CONTROLLER_ERROR(RESPONSES.TOKEN_ERROR);
		}

		return mysqlExtended.execute(`SELECT * FROM users WHERE id = '${userId}'`)
			.then(([userInfoResults]) => {
				if (userInfoResults.length !== 0) {
					return {
						userId,
						userInfo: {
							userName: userInfoResults[0].login,
							firstName: userInfoResults[0].first_name,
							lastName: userInfoResults[0].last_name,
							email: userInfoResults[0].e_mail,
							phone: userInfoResults[0].phone,
							type: userInfoResults[0].type,
						},
						token,
					};
				}

				throw GET_CONTROLLER_ERROR('Can not find any user info');
			});
	})
);

export default getUserInfoByToken;
