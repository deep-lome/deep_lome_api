import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const signOutRouter = (req) => mysqlExtended
	.execute(`DELETE FROM tokens WHERE token = '${req.header('Auth-Access')}'`)
	.catch(e => {
		throw GET_CONTROLLER_ERROR(e);
	});

export default signOutRouter;
