import getUserInfoByToken from './getUser/getUserInfoByToken';
import getBasketContent from '../basket/getBasketContent';

const checkTokenRoute = (req) => getUserInfoByToken(req.header('Auth-Access'))
	.then(data => getBasketContent(data.userId)
		.then(userBasket => ({ userInfo: data.userInfo, userBasket, token: data.token })));

export default checkTokenRoute;
