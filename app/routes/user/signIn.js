import crypto from 'crypto';
import { RESPONSES, GET_CONTROLLER_ERROR } from '../../init/constants';
import { mysqlExtended } from '../../init/connection';
import generateToken from '../../init/generateToken';
import insertToken from './token/insertToken';
import getUserInfoById from './getUser/getUserInfoById';
import getBasketContent from '../basket/getBasketContent';

const signInRoute = ({ body: { body: { userName, password } } }) => {
	const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
	const query = `SELECT * FROM users WHERE (e_mail = '${userName}' OR login = '${userName}') 
	AND (password = '${hashedPassword}')`;

	return mysqlExtended.execute(query)
		.then(async ([results]) => {
			if (results && results.length) {
				return { token: await generateToken(), userId: results[0].id };
			}

			throw GET_CONTROLLER_ERROR(RESPONSES.SIGN_IN_ERROR);
		}).then(({ token, userId }) => (insertToken(token, userId)))
		.then(data => getUserInfoById(data.userId)
			.then(userInfo => getBasketContent(data.userId)
				.then(userBasket => ({ userInfo, userBasket, token: data.token }))));
};

export default signInRoute;
