import crypto from 'crypto';
import { RESPONSES, GET_CONTROLLER_ERROR } from '../../init/constants';
import { mysqlExtended } from '../../init/connection';

const signUpRoute = ({ body: { body: {
	userName, password, firstName, lastName, phone, email,
} } }) => {
	const hashedPassword = crypto.createHash('sha256').update(password).digest('hex');
	const query = `INSERT INTO users (login, password, type, first_name, last_name, phone, e_mail) 
	VALUES ('${userName}', '${hashedPassword}', 'customer', '${firstName}', '${lastName}', '${phone}', '${email}')`;

	return mysqlExtended.execute(query).catch(() => {
		throw GET_CONTROLLER_ERROR(RESPONSES.SIGN_UP_ERROR);
	});
};

export default signUpRoute;
