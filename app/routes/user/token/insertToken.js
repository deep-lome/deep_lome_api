import { mysqlExtended } from '../../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../../init/constants';

const insertToken = (token, userId) => mysqlExtended
	.execute(`INSERT INTO tokens (user_id, token) VALUES (${userId}, '${token}') 
	ON DUPLICATE KEY UPDATE token = '${token}'`)
	.then(() => ({ token, userId }))
	.catch(e => {
		throw GET_CONTROLLER_ERROR(e);
	});

export default insertToken;
