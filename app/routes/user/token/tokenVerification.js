import { mysqlExtended } from '../../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../../init/constants';

const tokenVerification = (token) => mysqlExtended
	.execute(`SELECT * FROM tokens WHERE (token = '${token}' 
	AND DATEDIFF(NOW(), creation_date) < 1)`)
	.then(([list]) => {
		if (list && list.length > 0) {
			return list[0].user_id;
		}

		throw GET_CONTROLLER_ERROR('Access token expired', undefined, { status: 401 });
	});

export default tokenVerification;
