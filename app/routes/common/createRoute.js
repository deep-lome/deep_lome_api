import { getSuccessCallBack, getErrorCallBack } from './responsesCallBacks';
import { ERROR_TYPES, RESPONSES } from '../../init/constants';

const errorHanlder = (e, res) => {
	if (e.type === ERROR_TYPES.WARNING) {
		getErrorCallBack(res)(RESPONSES.GET_ERROR_RESPONSE(e.error, e.config.status));
	}
	if (e.type === ERROR_TYPES.FATAL) {
		getErrorCallBack(res)(RESPONSES.GET_ERROR_RESPONSE(e.error, 500));
	}
};

export default class RouterCreator {
	constructor(app) {
		this.app = app;
		this.createRoute = this.createRoute.bind(this);
	}

	createRoute(routePath, method, controller) {
		return this.app[method](routePath, (req, res) => {
			const handler = (data) => controller(req, res, data)
				.then(getSuccessCallBack(res))
				.catch((e) => errorHanlder(e, res));

			return handler();
		});
	}
}
