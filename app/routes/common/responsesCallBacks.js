import { RESPONSES } from '../../init/constants';

export const getSuccessCallBack = (response) => (data) => {
	response.status(data.status || 200);
	response.send(RESPONSES.GET_SUCCESS_RESPONSE(data));
};

export const getErrorCallBack = (response) => (error) => {
	response.status(error.status || 400);
	response.send(error);
};
