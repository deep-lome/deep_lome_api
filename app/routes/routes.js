import expressServer from '../init/expressServer';
import RouterCreator from './common/createRoute';
import signInRoute from './user/signIn';
import signUpRoute from './user/signUp';
import signOutRoute from './user/signOut';
import checkTokenRoute from './user/checkTokenRoute';
import softwareListRoute from './software/softwareList';
import changeBasketContentRoute from './basket/changeBasketContent';
import clearBasketContentRoute from './basket/clearBasketContent';
import sendOrderDataRouter from './oder/sendOrderDataRouter';
import { ROUTES, REQUEST_METHODS } from '../init/constants';

const routes = () => {
	const app = expressServer();
	const { createRoute } = new RouterCreator(app);

	createRoute(ROUTES.SIGN_IN, REQUEST_METHODS.POST, signInRoute);
	createRoute(ROUTES.SIGN_UP, REQUEST_METHODS.POST, signUpRoute);
	createRoute(ROUTES.CHECK_TOKEN, REQUEST_METHODS.GET, checkTokenRoute);
	createRoute(ROUTES.GET_SOFTWARE_LIST, REQUEST_METHODS.GET, softwareListRoute);
	createRoute(ROUTES.SIGN_OUT, REQUEST_METHODS.GET, signOutRoute);
	createRoute(ROUTES.CHANGE_BASKET_CONTENT, REQUEST_METHODS.POST, changeBasketContentRoute);
	createRoute(ROUTES.CLEAR_BASKET_CONTENT, REQUEST_METHODS.POST, clearBasketContentRoute);
	createRoute(ROUTES.CLEAR_BASKET_CONTENT, REQUEST_METHODS.POST, clearBasketContentRoute);
	createRoute(ROUTES.SEND_ORDER_DATA, REQUEST_METHODS.POST, sendOrderDataRouter);

	return app;
};

export default routes;
