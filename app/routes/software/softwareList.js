import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const getSoftwareListRoute = () => {
	const query = `SELECT software.id, software.name AS product_name, 
	software.developer, software.category, software.description, 
	offers.name AS offer_name, offers.code, offers.properties, offers.price 
	FROM software LEFT JOIN offers ON software.id = offers.software_id`;

	return mysqlExtended.execute(query)
		.then(([list]) => {
			const products = {};

			list.forEach(item => {
				if (!products[item.id]) {
					const offers = list.filter(soft => soft.id === item.id);
					products[item.id] = {
						name: item.product_name,
						developer: item.developer,
						category: item.category,
						description: item.description,
						offers: offers.map(offer => ({
							name: offer.offer_name,
							code: offer.code,
							properties: offer.properties,
							price: offer.price,
						})),
					};
				}
			});

			return Object.values(products);
		})
		.catch(e => { throw GET_CONTROLLER_ERROR(e); });
};

export default getSoftwareListRoute;
