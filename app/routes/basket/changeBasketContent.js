import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const changeBasketContentRoute = ({ body: { body: { userName, content } } }) => {
	const query = `SELECT id FROM users WHERE login = '${userName}'`;

	return mysqlExtended.execute(query)
		.then(([usersIdList]) => mysqlExtended
			.execute(`DELETE FROM basket WHERE user_id = '${usersIdList[0].id}'`)
			.then(() => mysqlExtended
				.execute(`SELECT offers.id FROM offers WHERE code IN ('${content
					.map(item => item.code).join('\', \'')}')`)
				.then(([offers]) => {
					const rows = content
						.map((item, index) => `(${usersIdList[0].id}, ${offers[index].id}, ${item.count})`);
					mysqlExtended
						.execute(`INSERT INTO basket (user_id, offer_id, count) VALUES ${rows.join(', ')}`)
						.catch(e => { throw GET_CONTROLLER_ERROR(e); });
				})));
};

export default changeBasketContentRoute;
