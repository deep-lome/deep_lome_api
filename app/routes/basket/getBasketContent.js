import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const getBasketContent = (userId) => {
	const query = `SELECT software.name AS product_name, 
	offers.name AS offer_name, offers.code, offers.name, 
	offers.properties, offers.price, basket.count FROM basket 
	LEFT JOIN offers ON basket.offer_id = offers.id 
	LEFT JOIN software ON offers.software_id = software.id 
	WHERE user_id = '${userId}'`;

	return mysqlExtended.execute(query).then(([list]) => list
		.map(item => ({
			product: {
				name: `${item.product_name} ${item.offer_name}`,
				code: item.code,
				price: item.price,
				description: item.properties,
			},
			count: item.count,
		})))
		.catch(e => { throw GET_CONTROLLER_ERROR(e); });
};

export default getBasketContent;
