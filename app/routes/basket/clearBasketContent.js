import { mysqlExtended } from '../../init/connection';
import { GET_CONTROLLER_ERROR } from '../../init/constants';

const clearBasketContentRoute = ({ body: { body: { userName } } }) => {
	const query = `SELECT id FROM users WHERE login = '${userName}'`;

	return mysqlExtended
		.execute(query)
		.then(([usersIdList]) => mysqlExtended
			.execute(`DELETE FROM basket WHERE user_id = '${usersIdList[0].id}'`)
			.catch(e => { throw GET_CONTROLLER_ERROR(e); }));
};

export default clearBasketContentRoute;
